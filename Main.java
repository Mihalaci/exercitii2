package exercitiu2;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String[] tipMasina = {"Masina de oras", "Supermini", "Hatchback", "MPV", "Estate", "Coupe", "Crossover", "SUV", "Cabriolet"};
        int[] anProductie = {2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023};
        String[] culori = {"Alb", "Negru", "Albastru", "Rosu", "Violet", "Gri", "Maro", "Verde"};
        String[] model = {"model1", "model2", "model3", "model4", "model5", "model6", "model7", "model8", "model9"};
        int[] pretMasina = {15000, 17000, 32000};

        System.out.println("Bun venit la salon de masini!\n");

        System.out.println("Tipurile de masini disponibile in salon sunt: ");
        for (String tip : tipMasina) {
            System.out.println("- " + tip);
        }

        System.out.print("\nIntroduceti tipul de masina dorit: ");
        String tipDorit = scanner.nextLine();

        System.out.println("Anii disponibil: ");
        for (int an : anProductie) {
            System.out.println("- " + an);
        }

        System.out.print("Introduceti anul producerii: ");
        int anDorit = scanner.nextInt();
        scanner.nextLine();

        System.out.println("Culorile disponibile: ");
        for (String culoare : culori) {
            System.out.println("- " + culoare);
        }

        System.out.print("Culoarea dorita: ");
        String culoareDorita = scanner.nextLine();

        System.out.println("Modele disponibile: ");
        for (String modele : model) {
            System.out.println("- " + modele);
        }

        System.out.print("Modelul dorit: ");
        String modelDorit = scanner.nextLine();

        System.out.println("Preturi disponibile: ");
        for (int pret : pretMasina) {
            System.out.println("- " + pret);
        }

        System.out.print("Pret dorit: ");
        int pretDorit = scanner.nextInt();
        scanner.nextLine();

        System.out.print("Doriti sa achizitionati aceasta masina? (Da/Nu): ");
        String raspuns = scanner.nextLine();

        if (raspuns.equalsIgnoreCase("DA")) {
            String[] masinaAleasa = {tipDorit, String.valueOf(anDorit), modelDorit, culoareDorita, String.valueOf(pretDorit)};
            System.out.println("Felicitari ati achizitionat o masina!");
            System.out.println("Detaliile masinii: ");
            System.out.println("Tip masina: " + masinaAleasa[0]);
            System.out.println("Anul masinii: " + masinaAleasa[1]);
            System.out.println("Modelul masinii: " + masinaAleasa[2]);
            System.out.println("Culoarea masinii: " + masinaAleasa[3]);
            System.out.println("Pretul masinii: " + masinaAleasa[4]);
        } else {
            System.out.println("Va multumimi pentru vizita! O zi buna!");
        }
    }
}
